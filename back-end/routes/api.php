<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Use App\Products;

Route::get('productos', 'ProductsController@index');
Route::get('productos/{id}', 'ProductsController@show');
Route::post('productos', 'ProductsController@store');
Route::put('productos/{id}', 'ProductsController@update');
Route::delete('productos/{id}', 'ProductsController@delete');