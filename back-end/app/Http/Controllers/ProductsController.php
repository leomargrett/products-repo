<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Products;

class ProductsController extends Controller
{
    public function index()
    {
        return Products::all();
    }
 
    public function show($id)
    {
        return Products::find($id);
    }

    public function store(Request $request)
    {
        return Products::create(json_decode($request->getContent(), true));
    }

    public function update(Request $request, $id)
    {
        $product = Products::findOrFail($id);

        $updatedProduct = json_decode($request->getContent(), true);
        $product->update($updatedProduct);
        
        return $product;
    }

    public function delete(Request $request, $id)
    {
        $product = Products::findOrFail($id);
        $product->delete();

        return 204;
    }
}
