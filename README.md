Codigo de ejemplo desarrollado para Matias Sanchez 

- Aplicacion desarrollada con Angular 6 y Composer/laravel

Requisitos previos:
- Tener creada una mysql DB
- Modificar los siguientes datos en el archivo ./back-end/.env:
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=DB_NAME
DB_USERNAME=USER_NAME
DB_PASSWORD=USER_PASSWORD

- Dentro ./back-end ejecutar los comandos:
  * php artisan migrate // realiza una migracion para cargar las tablas de la DB
  * php composer install // Instala las dependencias del proyecto
  * php artisian serve // para levantar el servidor

- Dentro ./front-end ejecutar los comandos:
  * npm install/yard install // Descarga los paquetes y dependencias
  * ng serve // levanta el servidor, sino se puede compilar el codigo para levantarlo con un servidor como apache



