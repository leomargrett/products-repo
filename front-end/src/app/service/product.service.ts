import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Product } from '../product';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productsUrl = 'http://localhost:8000/productos';  // URL api productos

  constructor(
  	private http: HttpClient) { }

  /** GET: recupera todos los productos */
   getProducts(): Observable<Product[]> {
  	return this.http.get<Product[]>(this.productsUrl)
  	.pipe(
  	  tap(Product => this.log('Productos recuperados')),
      catchError(this.handleError('getProducts', []))
    );
  }

  /** GET: recupera un producto determinado */
  getProduct(id: number): Observable<Product> {
  	const url = `${this.productsUrl}/${id}`;
  	return this.http.get<Product>(url)
  	.pipe(
  	  tap(_ => this.log('Producto recuperado id=${id}')),
      catchError(this.handleError<Product>('getProduct id=${id}'))
    );
  }

  /** PUT: actualizar producto */
  updateProduct (product: Product): Observable<any> {
  	const url = `${this.productsUrl}/${product.id}`;
    return this.http.put(url, product, httpOptions).pipe(
      tap(_ => this.log(`Producto actualizado id=${product.id}`)),
      catchError(this.handleError<any>('updateProducto'))
    );
  }

  /** POST: agregar producto */
  addProduct (product: Product): Observable<Product> {
    return this.http.post<Product>(this.productsUrl, product, httpOptions).pipe(
      tap((product: Product) => this.log(`Producto insertado id=${product.id}`)),
      catchError(this.handleError<Product>('addProduct'))
    );
  }

  /** DELETE: eliminar producto */
  deleteProduct (id: number): Observable<Product> {
    const url = `${this.productsUrl}/${id}`;

    return this.http.delete<Product>(url, httpOptions).pipe(
      tap(_ => this.log(`Producto eliminado id=${id}`)),
      catchError(this.handleError<Product>('deleteProduct'))
    );
  }

  private log(message: string) {
    // TODO Implentar logging funcion 
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: enviar error a error logging
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
