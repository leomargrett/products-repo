export class Product{
  id: number;
  nombre: string;
  precio: number;
  descripcion: string;
  visitas: number;
}