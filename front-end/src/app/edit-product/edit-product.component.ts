import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Product } from '../product';
import { ProductService }  from '../service/product.service';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
 
  product: Product;

  constructor(
  	private route: ActivatedRoute,
    private productService: ProductService,
    private router: Router,
    private location: Location,
    private messageService: MessageService) { }

  ngOnInit() {
  	this.getProduct();
  }

  /* Recupera el producto */
  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(id)
      .subscribe(product => this.product = product);
  }

  /* Actuliza el producto */
  save(): void {
  	let valid = true;
  	if (this.product.nombre.trim().length == 0 ){
  		this.messageService.add('Debe completar el nombre del producto'); valid=false;
  	}
  	if (!this.product.precio || this.product.precio < 0 ){
  		this.messageService.add('Debe completar el precio del producto'); valid=false;
  	}
  	if (this.product.descripcion.trim().length == 0 ){
  		this.messageService.add('Debe completar la descripción del producto'); valid=false;
  	}
  	if (!valid)
  		return;
  	else
  		this.messageService.clear();
  	
    this.productService.updateProduct(this.product)
     .subscribe(_ => this.router.navigate(['/productos']));
  }

  /* Vuelve a la pagina anterior */
  goBack(): void {
  	this.messageService.clear();
    this.location.back();
  }

}
