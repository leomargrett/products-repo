import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductsComponent }      from './products/products.component';
import { ViewProductComponent }  from './view-product/view-product.component';
import { EditProductComponent }  from './edit-product/edit-product.component';

const routes: Routes = [
  { path: 'productos', component: ProductsComponent },
  { path: 'productos/:id', component: ViewProductComponent },
  { path: 'editar-producto/:id', component: EditProductComponent },
  { path: '', redirectTo: 'productos', pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
