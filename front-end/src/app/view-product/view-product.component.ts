import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Product } from '../product';
import { ProductService }  from '../service/product.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  product: Product;

  constructor(
  	private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private location: Location) { }

  ngOnInit() {
  	this.getProduct();
  }

  /* Recupera el producto */
  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(id)
      .subscribe(product => {this.product = product; this.product.visitas++; this.save() });
  }

  /* Actualiza la cantidad de visitas del producto */
  save(): void {
   this.productService.updateProduct(this.product)
     .subscribe(response => console.log(response));
  }

  /* Elimina el producto */
  delete(): void {
    if(confirm("Está seguro que quiere eliminar el producto?")) {
      this.productService.deleteProduct(this.product.id).subscribe(
        _ => this.router.navigate(['/productos'])
        );
    } 
  }

  /* Vuelve a la pagina anterior */
  goBack(): void {
    this.location.back();
  }

}
